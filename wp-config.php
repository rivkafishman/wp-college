<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_college');

/** MySQL database username */
define('DB_USER', 'college');

/** MySQL database password */
define('DB_PASSWORD', '321collegeDataBase');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/?e957XK{zd vM!_r)^B/%A0!7w|TUjEr!^O17(vcve wb2_,?}B4=oL#H_0IJ&T');
define('SECURE_AUTH_KEY',  'PwdPZ8>c- S1N2}|<V%ZJmSOrPIx5{u<vqTl(_lZt#i]/<*rcJhW_:O1h#m=6G16');
define('LOGGED_IN_KEY',    '+7l/fim-B;]X~ihYG?28<=L91x#[c-B~Wii/z>k! E9ST$HSST0NH, `I7bQ=&WR');
define('NONCE_KEY',        '8?(l]0YyK]O#s/Nq[-3_fN6 Vj44Ln #c2Y9npCcG06eC3}*zPWLM_c->@c.O_Wl');
define('AUTH_SALT',        'vfCoWi=A~/tx3{TPy&aVBY%khyV`.- sIx#okQn0G{-FbNgL]n Wyw8VNDEZ,W,D');
define('SECURE_AUTH_SALT', '}!Hz)G8qbPn.MI)1Q@RdblH4Z}t$xn_di1Iy~hC4+;rLJF%WXeEv1|Z#(*AI?FA5');
define('LOGGED_IN_SALT',   'zWo<{v_A }FKcyxy0(=JE^vo7{:vPL8a2)<x=qfUvh ]^NBm2/|?duw}Vsan!mgE');
define('NONCE_SALT',       't]1/rQ+w;*R{Eu368EY:.`8:gsY6!Ot$!4ye{eF55S@RK: A|3#7=uE:lL^2aV,.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
